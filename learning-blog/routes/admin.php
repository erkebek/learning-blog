<?php

use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => LaravelLocalization::setLocale()], function(){

    /* Auth */
    Route::group(['prefix' => '/auth'], function(){
        Route::get('/login', 'AuthController@loginForm')->name('login');
        Route::post('/login', 'AuthController@login')->name('login-attempt');
        Route::get('/register', 'AuthController@registerForm')->name('register');
        Route::post('/register', 'AuthController@register')->name('register-user');
        Route::get('/logout', 'AuthController@logout')->name('logout');
    });

    Route::group(['prefix' => '/admin', 'as' => 'admin.', 'middleware' => ['auth', 'role:Administrator|Editor']], function(){
        Route::get('/', 'AdminController@index')->name('index');


        /* Posts CRUD */
        Route::group(['prefix' => '/post', 'as' => 'post.'], function(){
            Route::get('/', 'PostController@index')->name('index');
            Route::get('/create', 'PostController@create')->name('create');
            Route::post('', 'PostController@store')->name('store');
            Route::get('/edit/{id}', 'PostController@edit')->name('edit');
            Route::post('/update/{id}', 'PostController@update')->name('update');
            Route::get('/delete/{id}', 'PostController@delete')->name('delete');
        });

        /* Post Categories CRUD */
        Route::group(['prefix' => '/post-category', 'as' => 'post_category.'], function(){
            Route::get('/', 'PostCategoryController@index')->name('index');
            Route::get('/create', 'PostCategoryController@create')->name('create');
            Route::post('', 'PostCategoryController@store')->name('store');
            Route::get('/edit/{id}', 'PostCategoryController@edit')->name('edit');
            Route::post('/update/{id}', 'PostCategoryController@update')->name('update');
            Route::get('/delete/{id}', 'PostCategoryController@delete')->name('delete');
        });

        /* User CRUD */
        Route::group(['prefix' => '/user', 'as' => 'user.'], function(){
            Route::get('/', 'UserController@index')->name('index');
            Route::get('/create', 'UserController@create')->name('create');
            Route::post('', 'UserController@store')->name('store');
            Route::get('/edit/{id}', 'UserController@edit')->name('edit');
            Route::post('/update/{id}', 'UserController@update')->name('update');
            Route::get('/delete/{id}', 'UserController@delete')->name('delete');
        });


        Route::group(['middleware' => ['role:Administrator']], function(){
            Route::get('/contact', 'ContactController@index')->name('contact.index');
            Route::get('/contact/edit/{id}', 'ContactController@edit')->name('contact.edit');
            Route::post('/contact/update/{id}', 'ContactController@update')->name('contact.update');
        });

        // Route::get('/admin/post', 'PostController@create')->name('post.create');
    });
});

