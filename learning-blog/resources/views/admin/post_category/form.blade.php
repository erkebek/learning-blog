<div class="card-body">
    <div class="form-group">
        <label>{{trans('t.title')}}</label>
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        <label>Түшүндүрмө</label>
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-check">
        {!! Form::hidden('is_important', 0) !!}
        {!! Form::checkbox('is_important', 1, null, ['class' => 'form-check-input', 'id' => 'is_important']) !!}
        <label class="form-check-label" for="is_important">Башкы бетке чыгаруу</label>
    </div>
    <div class="form-check">
        {!! Form::hidden('active', 0) !!}
        {!! Form::checkbox('active', 1, null, ['class' => 'form-check-input', 'id' => 'exampleCheck1']) !!}
        <label class="form-check-label" for="exampleCheck1">Активдүү</label>
    </div>
</div>
<!-- /.card-body -->

<div class="card-footer">
    <button type="submit" class="btn btn-success">Сактоо</button>
</div>
