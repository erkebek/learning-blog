<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;


class Post extends Model
{
    protected $fillable = [
        'title',
        'description',
        'content',
        'active',
        'is_editors_pick',
        'category_id',
        'views',
    ];

    public function scores()
    {
        return $this->hasMany(Score::class);
    }

    public function getTotalScoreAttribute()
    {
        $scores = $this->scores;
        if($scores->count() > 0){
            $sum = 0;
            foreach($scores as $score){
                $sum = $sum + $score->value;
            }
            $average = $sum / $scores->count();
            return number_format($average, 2);
        }else{
            return trans('t.not_rated');
        }
    }

    public function category()
    {
        return $this->belongsTo(PostCategory::class, 'category_id')->withDefault(['title' => trans('t.no_category')]);
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id')->withDefault([
            'name' => trans('t.no_author')
        ]);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'post_id')->whereNull('parent_comment_id');
    }

    public function allComments()
    {
        return $this->hasMany(Comment::class, 'post_id');
    }

    public function storeImage($file)
    {
        if($this->image){
            File::delete($this->image);
        }

        $folder = "assets/uploads/posts/{$this->id}";
        if(!file_exists($folder))
        {
            mkdir($folder, 0777, true);
        }

        $image_name = "image.jpg";

        $image_path = "{$folder}/{$image_name}";

        Image::make($file)->fit(1280, 720)->encode('jpg')->save($image_path);

        $this->image = $image_path;
        $this->save();
    }

    public function getImageAttribute($value)
    {
        if($value) return $value;
        return '/assets/uploads/posts/default_image.png';
    }

    public static function boot() {
	    parent::boot();

	    static::creating(function($item) {
            $item->user_id = auth()->user()->id;
	    });

	    static::updating(function($item) {
            $item->user_id = auth()->user()->id;
	    });

	    static::deleting(function($item) {
            if($item->image){
                File::deleteDirectory("assets/uploads/posts/{$item->id}");
            }
	    });
	}
}
