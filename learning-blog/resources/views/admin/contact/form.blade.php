<div class="card-body">
    @if($errors->any())
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
            <li>{{$error}}</li>
        @endforeach
    </div>
    @endif
    <div class="form-group">
        <label>{{trans('t.name')}}</label>
        {!! Form::text('name', null, ['class' => 'form-control', 'readonly' => 'true']) !!}
    </div>
    <div class="form-group">
        <label>{{trans('t.value')}}</label>
        {!! Form::text('value', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-check">
        {!! Form::hidden('active', 0) !!}
        {!! Form::checkbox('active', 1, null, ['class' => 'form-check-input', 'id' => 'exampleCheck1']) !!}
        <label class="form-check-label" for="exampleCheck1">{{trans('t.active')}} </label>
    </div>
</div>
<!-- /.card-body -->

<div class="card-footer">
    <button type="submit" class="btn btn-success">{{trans('t.save')}}</button>
</div>
