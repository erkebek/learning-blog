<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.index');
    }

    public function lang()
    {
        //TODO: implement locale change
        if(app()->getLocale() == 'ky'){
            app()->setLocale('ru');
        }else{
            app()->setLocale('ky');
        }
        // dd(app()->getLocale());
        return redirect()->back();
    }
}
