@extends('admin.layouts.base')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{trans('t.posts')}}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">{{trans('t.main')}}</a></li>
              <li class="breadcrumb-item active">{{trans('t.posts')}}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                  <h3 class="card-title">{{trans('t.new_post')}}</h3>
                  <div class="float-right">
                      <a href="{{route('admin.post.index')}}" class="btn btn-primary">{{trans('t.all_posts')}}</a>
                  </div>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                {!! Form::model($row, ['role' => 'form', 'method' => 'POST', 'route' => ['admin.post.store',
                $row->id], 'enctype' => 'multipart/form-data']) !!}
                @include('admin.post.form')
                {!! Form::close() !!}
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('scripts')
    @include('admin.partials.editor')
@endsection
