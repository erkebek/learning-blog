@extends('web.layouts.base')

@section('title', trans('t.main_page'))

@section('content')


<div class="site-section py-0">
    <div class="owl-carousel hero-slide owl-style">

        @foreach($editorsPickedPosts as $row)
        <div class="site-section">
            <div class="container">
                <div class="half-post-entry d-block d-lg-flex bg-light">
                    <div class="img-bg" style="background-image: url({{asset($row->image)}})"></div>
                    <div class="contents">
                        <span class="caption">Editor's Pick</span>
                        <h2><a href="{{route('web.post.show', $row->id)}}">{{$row->title}} </a></h2>
                        <p class="mb-3">
                            {{$row->description}}
                        </p>

                        <div class="post-meta">
                            <span class="d-block">
                                <a href="#">{{$row->author->name}} </a> in <a href="#">{{$row->category->title}} </a>
                            </span>
                            <span class="date-read" style="text-transform: capitalize">{{$row->created_at->isoFormat('MMM DD')}} <span class="mx-1">&bullet;</span> 3 min read <span
                                    class="icon-star2"></span></span>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        @endforeach

    </div>
</div>


<div class="site-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="section-title">
                    <h2>{{trans('t.recent_news')}} </h2>
                </div>

                @foreach($recentPosts as $row)
                <div class="post-entry-2 d-flex">
                    <div class="thumbnail order-md-2" style="background-image: url({{asset($row->image)}})"></div>
                    <div class="contents order-md-1 pl-0">
                        <h2><a href="{{route('web.post.show', $row->id)}}">{{$row->title}} </a></h2>
                        <p class="mb-3">{{$row->description}} </p>
                        <div class="post-meta">
                            <span class="d-block"><a href="#">{{$row->author->name}} </a> in <a href="#">{{$row->category->title}} </a></span>
                            <span class="date-read">{{$row->created_at->isoFormat('MMM DD')}} <span class="mx-1">&bullet;</span> 3 min read <span
                                    class="icon-star2"></span></span>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="col-lg-3">
                <div class="section-title">
                    <h2>Popular Posts</h2>
                </div>

                @foreach ($popularPosts as $row)
                <div class="trend-entry d-flex">
                    <div class="number align-self-start">{{ "0{$loop->iteration}" }} </div>
                    <div class="trend-contents">
                        <h2><a href="{{route('web.post.show', $row->id)}}">{{$row->title}} </a></h2>
                        <div class="post-meta">
                            <span class="d-block"><a href="#">{{$row->author->name}} </a> in <a href="#">{{$row->category->title}} </a></span>
                            <span class="date-read">{{$row->created_at->isoFormat('MMM DD')}} <span class="mx-1">&bullet;</span> 3 min read <span
                                    class="icon-star2"></span></span>
                        </div>
                    </div>
                </div>
                @endforeach

                <p>
                    <a href="#" class="more">See All Popular <span class="icon-keyboard_arrow_right"></span></a>
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                {!! $recentPosts->links() !!}
            </div>
        </div>
    </div>
</div>

@endsection
