<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function index()
    {
        $rows = User::all();
        return view('admin.user.index', compact('rows'));
    }

    public function cabinet()
    {
        $posts = Post::where('active', true)->where('user_id', auth()->user()->id)->get();
        return view('admin.post.index');
    }

    public function create()
    {
        $row = new User;
        $roles = Role::all()->pluck('name', 'id');
        $roles->prepend('Ролу жок', -1);
        return view('admin.user.create', compact('row', 'roles'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'last_name' => '',
            'email' => 'required',
            'password' => 'required|confirmed',
            'active' => '',
        ]);
        $user = User::create($request->all());
        $this->setPasswordAndAssingRoles($user, $request);

        return redirect()->route('admin.user.index');
    }

    public function edit($id)
    {
        $row = User::findOrFail($id);
        $roles = Role::all()->pluck('name', 'id');
        $roles->prepend('Ролу жок', -1);
        return view('admin.user.edit', compact('row', 'roles'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'last_name' => '',
            'email' => 'required',
            'password' => 'nullable|confirmed',
            'active' => '',
        ]);
        $user = User::findOrFail($id);
        $this->setPasswordAndAssingRoles($user, $request);

        return redirect()->route('admin.user.index');
    }

    public function setPasswordAndAssingRoles(User $user, Request $request)
    {
        $formFields = $request->all();
        $formFields['password'] = Hash::make($request->get('password'));
        $user->update($formFields);
        if($request->get('role_id')){
            foreach($user->roles as $role){
                $user->removeRole($role);
            }
            if($request->get('role_id') != -1){
                $role = Role::findOrFail($request->get('role_id'));
                if(!$user->hasRole($role)){
                    $user->assignRole($role);
                }
            }
        }
        return $user;
    }

    public function delete($id){
        $post = User::findOrFail($id);
        $post->delete();
        return redirect()->back();
    }
}
