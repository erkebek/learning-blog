<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Post;
use App\Models\PostCategory;
use App\Models\Score;
use Illuminate\Http\Request;

class WebController extends Controller
{
    public function index()
    {
        $editorsPickedPosts = Post::where('active', true)
            ->where('is_editors_pick', true)
            ->orderBy('created_at', 'DESC')->get();
        $recentPosts = Post::where('active', true)
            ->orderBy('created_at', 'DESC')->paginate(4);
        $popularPosts = Post::where('active', true)
            ->orderBy('views', 'DESC')->limit(4)->get();
        return view('web.index', compact('editorsPickedPosts', 'recentPosts', 'popularPosts'));
    }

    public function post($id)
    {
        $row = Post::findOrFail($id);
        $row->increment('views');
        return view('web.post.show', compact('row'));
    }

    public function postComment(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'message' => 'required',
            'parent_id' => 'nullable',
        ]);
        $formFields = $request->all();
        $formFields['post_id'] = $id;
        $formFields['user_id'] = auth()->user()->id;
        Comment::create($formFields);

        return redirect()->back();
    }

    public function postRate(Request $request, $postId)
    {
        $this->validate($request, [
            'value' => 'required',
            'note' => 'nullable',
        ]);
        $userId = auth()->user()->id;

        $values = $request->only('value', 'note');
        $values['post_id'] = $postId;
        $values['user_id'] = $userId;
        if(Score::where('user_id', $userId)->where('post_id', $postId)->first()){
            return response()->json([
                'message' => trans('t.already_rated'),
            ], 403);
        }
        $score = Score::updateOrCreate(['user_id' => $values['user_id']], $values);

        return response()->json([
            'message' => trans('t.successfully_rated')
        ], 200);
    }

    public function category($id)
    {
        $category = PostCategory::findOrFail($id);

        $categoryPosts = Post::where('category_id', $id)->where('active', true)->paginate(5);
        return view('web.category.show', compact('category', 'categoryPosts'));
    }
}
