@extends('admin.layouts.base')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Посттор</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Башкы</a></li>
              <li class="breadcrumb-item active">Посттор</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Бардык посттор</h3>
                  <div class="float-right">
                      <a href="/admin/post/create" class="btn btn-primary">Пост кошуу</a>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Аталышы</th>
                      <th>Автору</th>
                      <th>Категорисы</th>
                      <th>Көрүүлөр</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($rows as $item)
                        <tr>
                            <td>{{$item->id}}</td>
                            <td>{{$item->title}}</td>
                            <td>{{$item->author->name}}</td>
                            <td>{{$item->category->title}}</td>
                            <td>{{$item->views}}</td>
                            <td>
                                <a href="{{route('admin.post.edit', $item->id)}}" class="btn btn-warning">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a onclick="return confirm('{{trans('t.confirm_delete')}}')" href="{{route('admin.post.delete', $item->id)}}" class="btn btn-danger">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                      <th>ID</th>
                      <th>Аталышы</th>
                      <th>Автору</th>
                      <th>Категорисы</th>
                      <th>Көрүүлөр</th>
                      <th></th>
                    </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
