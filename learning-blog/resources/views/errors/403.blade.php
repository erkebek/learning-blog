@extends('web.layouts.base')

@section('content')

<div class="site-section">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2>#403 {{trans('t.error_forbidden')}} </h2>
                <p>{{trans('t.page_you_are_looking_for_forbidden')}} </p>
            </div>
        </div>
    </div>
</div>
@endsection
