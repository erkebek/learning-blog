<?php

use App\Models\Contact;
use Illuminate\Database\Seeder;

class ContactTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Contact::truncate();
        Contact::create([
            'name' => 'Facebook',
            'value' => 'https://fb.com/sample_page',
            'active' => true,
            'icon' => 'icon-facebook',
        ]);

        Contact::create([
            'name' => 'Instagram',
            'value' => 'https://instagram.com/sample_page',
            'active' => true,
            'icon' => 'icon-instagram',
        ]);

        Contact::create([
            'name' => 'Twitter',
            'value' => 'https://twitter.com/sample_page',
            'active' => true,
            'icon' => 'icon-twitter',
        ]);

        Contact::create([
            'name' => 'WhatsApp',
            'value' => 'https://wa.me/99655555555',
            'active' => true,
            'icon' => 'icon-whatsapp',
        ]);

        Contact::create([
            'name' => 'Telegram',
            'value' => 'https://t.me/sample_page',
            'active' => true,
            'icon' => 'icon-telegram',
        ]);
    }
}
