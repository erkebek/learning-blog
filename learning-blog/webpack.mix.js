const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/**
 * Web styles
 */
mix.styles(
    [
        "resources/fonts/icomoon/style.css",
        "resources/css/bootstrap.min.css",
        "resources/css/jquery-ui.css",
        "resources/css/owl.carousel.min.css",
        "resources/css/owl.theme.default.min.css",
        "resources/css/owl.theme.default.min.css",
        "resources/css/jquery.fancybox.min.css",
        "resources/css/bootstrap-datepicker.css",
        "resources/fonts/flaticon/font/flaticon.css",
        "resources/css/aos.css",
        "resources/css/jquery.mb.YTPlayer.min.css",
        "resources/css/style.css",
    ],
    "public/assets/css/app.css"
);

mix.sass('resources/sass/app.scss', 'public/assets/css/dist.css');

/**
 * Web scripts
 */
mix.scripts(
    [
        "resources/js/jquery-3.3.1.min.js",
        "resources/js/jquery-migrate-3.0.1.min.js",
        "resources/js/jquery-ui.js",
        "resources/js/popper.min.js",
        "resources/js/bootstrap.min.js",
        "resources/js/owl.carousel.min.js",
        "resources/js/jquery.stellar.min.js",
        "resources/js/jquery.countdown.min.js",
        "resources/js/bootstrap-datepicker.min.js",
        "resources/js/jquery.easing.1.3.js",
        "resources/js/aos.js",
        "resources/js/jquery.fancybox.min.js",
        "resources/js/jquery.sticky.js",
        "resources/js/jquery.mb.YTPlayer.min.js",
        "resources/js/main.js",
    ],
    "public/assets/js/app.js"
).version();


/**
 * Admin styles
 */
mix.styles(
    [
        "resources/admin/plugins/fontawesome-free/css/all.min.css",
        "resources/admin/dist/css/adminlte.min.css",
    ],
    "public/assets/admin/css/app.css"
);

/**
 * Admin scripts
 */
mix.scripts(
    [
        "resources/admin/plugins/jquery/jquery.min.js",
        "resources/admin/plugins/bootstrap/js/bootstrap.bundle.min.js",
        "resources/admin/dist/js/adminlte.js",
    ],
    "public/assets/admin/js/app.js"
).version();
