<?php

use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => LaravelLocalization::setLocale(), 'as' => 'web.', 'middleware' => []], function(){
    Route::get('/', "WebController@index")->name('home');
    Route::get('/post/{id}', "WebController@post")->name('post.show');

    Route::get('/category/{id}', "WebController@category")->name('category.show');

    Route::group(['middleware' => ['auth']], function(){
        Route::post('/post/{id}/comment', 'WebController@postComment')->name('post.comment');

        Route::post('/post/{id}/rate', 'WebController@postRate')->name('post.rate');
    });

});
